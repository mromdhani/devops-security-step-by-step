# Git Security
---

## Is Git Secure ?
Nearly every developer uses Git version control at some point or another. Most development projects today use the Git source code control system, where Git repository metadata and packed versions of the files in the repository are stored in a hidden subdirectory named ".git".

The repository metadata and content can give an attacker helpful information for further attacks. The repository not only potentially reveals the web page source code -- passwords, secret tokens or confidential customer data could also be exposed. HTTP authentication with passwords for checkouts should be generally avoided for better Git repository security. Public key authentication with SSH public/private key pairs is much safer.

**Four Recent  GitHub Security Breaches**
1. In _October 2019_, [Starbucks developers left an API key in a public GitHub repo](https://www.bleepingcomputer.com/news/security/starbucks-devs-leave-api-key-in-github-public-repo/). This vulnerability was discovered through a bug bounty platform — and Starbucks paid a bounty to remediate it.
2. In _September 2019_, [CircleCI announced a security breach](https://www.scmagazine.com/home/security-news/data-breach/circlci-data-breach-exposed-customer-github-and-bitbucket-logins/). This breach exposed customer login information on both GitHub and Bitbucket Git repositories. CircleCI is now evaluating two-factor authentication measures to prevent a future breach.

3. In _August 2019_, GitHub was called out in a lawsuit following the [Capital One breach](https://nakedsecurity.sophos.com/2019/08/06/github-encourages-hacking-says-lawsuit-following-capital-one-breach/). The reason? Allowing social security numbers to be stored in a Git repository. The lawsuit also cited the public GitHub repository “Awesome Hacking” for encouraging bad behavior.

4. In _May 2019_, [hackers began wiping Git repositories](https://www.zdnet.com/article/a-hacker-is-wiping-git-repositories-and-asking-for-a-ransom/) and replacing them with ransom demands. Those affected were told to pay up or risk the hackers releasing the code. Hundreds of developers on GitHub, GitLab, and Bitbucket were affected. However, developers were encouraged to contact GitHub, GitLab, and Bitbucket support teams to recover their repositories (instead of rewarding the hackers)

## Git Security Best Practices

There are a lot of valuable resources dealing with the best Git Security. For example, these are interesting practices by [GitHub](https://help.github.com/en/enterprise/2.19/admin/user-management/best-practices-for-user-security) and by [Synk](https://snyk.io/blog/ten-git-hub-security-best-practices/).

#### Do not commit sensitive information
Committing sensitive information can lead to critical situations and happens more often than you might think. In particular, with the emergence of cloud providers, continuous integration online services and the Infrastructure as Code (IaC) paradigm, our source codes now contain access tokens, IP addresses, etc. 

Even if we suppose that your .gitignore contains all the potential sensitive files of your project, it won’t stop to inadvertency commit an access token in a file legitimately tracked by Git. 

#### Protect the access to your Git repositories
Another critical security issue you should guard against is unauthorised access to your Git repositories. Git by itself only takes care of keeping track of the changes occurring inside a repository. When you want to share your work, you need to expose this repository, and you should do it in a secure manner, otherwise you can get into trouble.

From a network perspective, your best choice is to rely on HTTPS or SSH to encrypt your traffic and authenticate the remote server. When using SSH (recommended), your private key should be protected by a passphrase and never leave your computer. If you have several development workstations, create one pair of SSH keys for each of them. 

#### Sign your work
Git enables you to cryptographically sign your work using GPG keys. Signing your work is important because anyone can choose the name and the email address of the author displayed in the Git commits. Both Git commits and tags can be signed and even the push requests since version 2.2.0. However, in practice, it is pointless to sign each commit since many operations would invalidate those signatures (see Linus Torvalds’ arguments on this point). Only Git tags need to be signed.

#### Keep Git and related tools up to date
As is true of software in general, Git is not perfect and can be impacted by vulnerabilities. The recent CVE-2018-11235 is a good example of this, allowing an attacker to craft a malicious Git repository leading to a remote code execution when cloned by someone. 

## Practice 1: Using SSH Keys
The SSH protocol provides this security and allows you to authenticate to the a Git remote server without supplying your username or password each time.
The only requirement is to have the OpenSSH client installed on your system. This comes pre-installed on GNU/Linux and macOS, but not on Windows.

> The easiest way to install Git and the SSH client on Windows is [Git for Windows](https://gitforwindows.org/). It provides a Bash emulation (Git Bash) used for running Git from the command line and the ssh-keygen command that is useful to create SSH keys as you’ll learn below.

This practice step uses GitLab. It is highly inspired from this [link](https://docs.gitlab.com/ee/ssh/).
GitLab supports RSA, DSA, ECDSA, and ED25519 keys. Their difference lies on the signing algorithm, and some of them have advantages over the others.

####  Generating a new SSH key pair

To create a new SSH key pair:

  - Open a terminal on Linux or macOS, or Git Bash / WSL on Windows.
Generate a new RSA SSH key pair:

    ``` shell
    ssh-keygen -t RSA -C "email@example.com"
    ```
    The `-C` flag adds a comment in the key in case you have multiple of them and want to tell which is which. It is optional.
  - Next, you will be prompted to input a file path to save your SSH key pair to. If you don’t already have an SSH key pair and aren’t generating a [deploy key](https://docs.gitlab.com/ee/ssh/#deploy-keys), use the suggested path by pressing `Enter`. Using the suggested path will normally allow your SSH client to automatically use the SSH key pair with no additional configuration.
  If you already have an SSH key pair with the suggested file path, you will need to input a new file path and [declare what host](https://docs.gitlab.com/ee/ssh/#working-with-non-default-ssh-key-pair-paths) this SSH key pair will be used for in your `~/.ssh/config` file.

  - Once the path is decided, you will be prompted to input a password to secure your new SSH key pair. It’s a best practice to use a password, but it’s not required and you can skip creating it by pressing `Enter` twice.
  If, in any case, you want to add or change the password of your SSH key pair, you can use the `-p` flag:

    ```shell
    ssh-keygen -p -f <keyname>
    ```

####  Adding an SSH key to your GitLab account
1. Copy your **public** SSH key to the clipboard by using one of the commands below depending on your Operating System:

   Git Bash on Windows:

    ```shell
    cat ~/.ssh/id_rsa.pub.pub | clip
    ```
    You can also open the key in a graphical editor and copy it from there, but be careful not to accidentally change anything.

2. Add your **public** SSH key to your GitLab account by:

    - Clicking your avatar in the upper right corner and selecting **Settings**.
    - Navigating to **SSH Keys** and pasting your **public** key from the clipboard into the **Key** field. If you:
      * Created the key with a comment, this will appear in the Title field.
      * Created the key without a comment, give your key an identifiable title like Work Laptop or Home Workstation.
    - Click the Add key button.

####   Testing that everything is set up correctly
To test whether your SSH key was added correctly, run the following command in your terminal (replacing gitlab.com with your GitLab’s instance domain):

```shell
ssh -T git@gitlab.com
```
You can also test your SSH configuration by cloning an existing Gitlab repository. Go to folder where you want to clone the repository and run your clone command.
```shell
git clone git@gitlab.com:mromdhani/gitlabci-step-by-step.git
```
The first time you connect to GitLab via SSH, you will be asked to verify the authenticity of the GitLab host you are connecting to. For example, when connecting to GitLab.com, answer yes to add GitLab.com to the list of trusted hosts:

```
The authenticity of host 'gitlab.com (35.231.145.151)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitlab.com' (ECDSA) to the list of known hosts.
```
If you are working with non default SSHkeys, additional configuration information is given [here](https://docs.gitlab.com/ee/ssh/#working-with-non-default-ssh-key-pair-paths).

## Practice 2 : Signing commits with GPG
GnuPG, also known as GPG, is a complete and free implementation of the OpenPGP standard as defined by RFC4880 (also known as PGP). GnuPG allows you to encrypt and sign your data and communications; it features a versatile key management system, along with access modules for all kinds of public key directories. GnuPG, also known as GPG, is a command line tool with features for easy integration with other applications. A wealth of frontend applications and libraries are available. GnuPG also provides support for S/MIME and Secure Shell (ssh).

  > Since its introduction in 1997, GnuPG is Free Software (meaning that it respects your freedom). It can be freely used, modified and distributed under the terms of the GNU General Public License .

You can use a GPG key to **sign Git commits** made in a GitLab repository. Signed
commits are labeled Verified if the identity of the committer can be
**verified**. To verify the identity of a committer, GitLab requires their public GPG key.

#### How GitLab handles GPG
GitLab uses its own keyring to verify the GPG signature. It does not access any public key server.
For a commit to be verified by GitLab:

- The committer must have a GPG public/private key pair.
- The committer's public key must have been uploaded to their GitLab account.
- One of the emails in the GPG key must match a verified email address used by the committer in GitLab.
- The committer's email address must match the verified email address from the GPG key.

#### Generating a GPG key

If you don't already have a GPG key, the following steps will help you get
started:

- [Install GPG](https://www.gnupg.org/download/index.html) for your operating system.
If your Operating System has `gpg2` installed, replace `gpg` with `gpg`2 in
the following commands.

- Generate the private/public key pair with the following command, which will
spawn a series of questions:
```shell
gpg --full-gen-key
```
Note:
In some cases like Gpg4win on Windows and other macOS versions, the command
here may be gpg --gen-key.

The first question is which algorithm can be used. Select the kind you want
or press Enter to choose the default (RSA and RSA).

- Use the following command to list the private GPG key you just created:
```shell
gpg --list-secret-keys --keyid-format LONG <your_email>
```

- Replace `<your_email>` with the email address you entered above.

```
Copy the GPG key ID that starts with sec. In the following example, that's
30F2B65B9246B6CA:
sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]
      D5E4F29F3275DC0CDA8FFC8730F2B65B9246B6CA
uid                   [ultimate] Mr. Robot <your_email>
ssb   rsa4096/B7ABC0813E4028C0 2017-08-18 [E]

```

Export the public key of that ID (replace your key ID from the previous step):
```shell
gpg --armor --export 30F2B65B9246B6CA
```
####  Adding a GPG key to your account
Note:
Once you add a key, you cannot edit it, only remove it. In case the paste
didn't work, you'll have to remove the offending key and re-add it.
You can add a GPG key in your profile's settings:
- On the upper right corner, click on your avatar and go to your Settings.
- Navigate to the GPG keys tab and paste your public key in the 'Key'
box.
- Finally, click on Add key to add it to GitLab. You will be able to see
its fingerprint, the corresponding email address and creation date.

####  Associating your GPG key with Git
After you have created your GPG key and added it to your account, it's time to tell Git which key to use.

1. Use the following command to list the private GPG key you just created:
```shell
gpg --list-secret-keys --keyid-format LONG <your_email>
```
Replace `<your_email>` with the email address you entered above.

2. Copy the GPG key ID that starts with `sec`. In the following example, that's
`30F2B65B9246B6CA`:
```
sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]
      D5E4F29F3275DC0CDA8FFC8730F2B65B9246B6CA
uid                   [ultimate] Mr. Robot <your_email>
ssb   rsa4096/B7ABC0813E4028C0 2017-08-18 [E]
```

3. Tell Git to use that key to sign the commits:
```
git config --global user.signingkey 30F2B65B9246B6CA
```
Replace `30F2B65B9246B6CA` with your GPG key ID.

####   Signing commits
After you have created your GPG key and added it to your account, you can start signing your commits:

1. Commit like you used to, the only difference is the addition of the -S flag:
```shell
git commit -S -m "My commit msg"
```
> Windows 10 : Troubleshooting.
> If the previous command does not work, you may tell to git the full path for the gpg command as follows:
>  `config --global gpg.program "C:/Program Files (x86)/GNU/GnuPG/gpg2.exe"`
2. Enter the passphrase of your GPG key when asked.
3. Push to GitLab and check that your commits are verified.

If you don't want to type the `-S` flag every time you commit, you can tell Git
to sign your commits automatically:
```shell 
git config --global commit.gpgsign true
```
####  Verifying commits

1. Within a project or merge request, navigate to
the Commits tab. Signed commits will show a badge containing either
"Verified" or "Unverified", depending on the verification status of the GPG
signature.
![verif](images/signatureverified.png)
1. By clicking on the GPG badge, details of the signature are displayed.
2. ![badge](images/badge.png)

## Practice 3: Protecting secrets from being committed
Secrets are committed often, and are discoverable very quickly, likely before the affected parties have time to react. Attackers can, and have, used similar techniques to identify secrets and use them for malicious purposes. But fortunately for us, these and other techniques can be used by organizations to monitor their own repositories for secrets being committed.

While you can use any secret scanning tool such as [truffleHog](https://github.com/dxa4481/truffleHog) or [gitleaks](https://github.com/zricethezav/gitleaks) in a pre-commit hook, other tools such as [detect-secrets](https://github.com/Yelp/detect-secrets) from Yelp or [git-secrets](https://github.com/awslabs/git-secrets) from Amazon Web Services make this step even easier by handling the installation for you.

If you’re using GitHub Enterprise, you have the option of using pre-receive hooks to run secret scanning tools before accepting the commit into the remote repository. Gitlab offers similar hooks, including a predefined blacklist designed to catch secrets being committed without the need for a separate scanning tool. 

#### Using [Gitleaks](https://github.com/zricethezav/gitleaks)

Gitleaks is a tool for auditing git repos for secrets. Gitleaks provides a way for you to find unencrypted secrets and other unwanted data types in git repositories. As part of its core functionality, it provides:

- Audits for uncommitted changes
- Github and Gitlab support including support for bulk organization and repository owner (user) repository scans, as well as pull/merge request scanning for use in common CI workflows.
- Support for private repository scans, and repositories that require key based authentication
- Output in JSON formats for consumption in other reporting tools and frameworks
Externalised configuration for environment specific customisation including regex rules
- High performance through the use of src-d's go-git framework
  
**Installing Gitleaks**
Written in Go, gitleaks is available in binary form for many popular platforms and OS types from the releases page. Alternatively, executed via Docker or it can be installed using Go directly, as per the below;

_MacOS_
```shell
brew install gitleaks
```
_Docker_
```shell
docker pull zricethezav/gitleaks
```
_Go_
Ensure `GO111MODULE=on` is set as an env var

```
go get github.com/zricethezav/gitleaks/v3@latest
```
**GitLeaks Docker usage examples**
Run gitleaks against:

Public repository
```shell
docker run --rm --name=gitleaks zricethezav/gitleaks -v -r https://github.com/zricethezav/gitleaks.git
```
Local repository already cloned into `/tmp/`
```shell
docker run --rm --name=gitleaks -v /tmp/:/code/ zricethezav/gitleaks -v --repo-path=/code/gitleaks
```
By default repos cloned to memory. Using `--disk` for clone to disk or you can quickly out of memory.

_Exit Codes_
Gitleaks provides consistent exist codes to assist in automation workflows such as CICD platforms and bulk scanning.

`0`: no leaks
`1`: leaks present
`2`: error encountered

**Practical exercices with GitLeak**

- Scanning Gitlab repositories for Secrets
    - Experiment scanning the vulnerable demo repository provided in this example. 
    ```shell
    docker run --rm --name=gitleaks zricethezav/gitleaks -v -r https://github.com/zricethezav/gitleaks.git 
    ```
    - Redirect the result in a file by adding the following redirection at the end of the previous command: `> resultat1`
    -  Experiment scanning one of your Gitlab repositories and redirect the resulta to a new result file. 
    -  Add a new file to your Gitlab repository that contains the following content : a fake AWS Secret Key. Rescan again your repository and analyse the results.
        ```
        macle=AKIAIO5FODNN7EXAMPLE 
        ```
- Scanning for Secrets within a CI/CD Pipeline
    -  Start Jenkins locally using the following command (for the first installation, follow the wizar to create an admin user and install the default plugins)
        ```shell
        java -jar jenkins.war --httpPort=8080 
        ```
    - Add the Docker Plugin to Jenkins (Menu Manage jenkins -> Manage Plugins) 
    - Create a Pipeline project an a sample stage for scanning Git for secrets.  
        - This is an example of vulnerable repository :https://gitlab.com/mromdhani/demogit_g1
        - This is an example of a clean repository: https://gitlab.com/mromdhani/gitlabci-step-by-step
      This is an overview of stage:

        ```shell
        pipeline {
            agent any
            // tools { 
            // }
            stages {       
                stage ('Check-Git-Secrets') {
                    steps {
                        bat 'del secrets-in-git-report 2>nul'
                        bat 'docker pull zricethezav/gitleaks'
                        bat 'docker run --rm --name=gitleaks zricethezav/gitleaks -v -r https://gitlab.com/mromdhani/demogit_g1 > secrets-in-git-report'
                    }
                }
            }
} 
        ```