# Docker Security
---

## Scan Images for Vulnerabilities with CoreOS Clair

[Clair](https://github.com/quay/clair) is an Open Source project from CoreOS, designed to scan Docker Images for Security Vulnerabilities.

In this scenario you will learn how to deploy Clair and scan Docker Images from the Docker Hub and private registries for known Vulnerabilities.

Lab Stpes: 
- Interactive browser-based sceanrio:
https://www.katacoda.com/courses/docker-security/image-scanning-with-clair, by Ben Hall
  - **Difficulty** : Beginner
  - **Duration**: 10 Minutes

## Ignoring Files From Docker Build

In this scenario, we'll explore how you can ignore certain files from ending up inside a Docker image that can introduce security risks. This scenario also investigates how you can reduce the build time by ignoring which files are sent to the Docker Build Context.
Lab Stpes: 
- Interactive browser-based sceanrio:
https://www.katacoda.com/courses/docker-security/ignoring-files-docker-build, by Ben Hall
  - **Difficulty** : Beginner
  - **Duration**: 10 Minutes

## CGroups and Namespaces

In this scenario you will learn the foundations of CGroups (Control Groups) and Namespces to apply security restrictions to containers.

Below are some examples of the types of cgroups and namespaces that exist.

**CGroups Examples**
```shell
--cpu-shares
  --cpuset-cpus
  --memory-reservation
  --kernel-memory
  --blkio-weight (block  IO)
  --device-read-iops
  --device-write-iops
```  
**Namespace Examples**
```shell
Cgroup      CLONE_NEWCGROUP   Cgroup root directory
  IPC         CLONE_NEWIPC      System V IPC, POSIX message queues
  Network     CLONE_NEWNET      Network devices, stacks, ports, etc.
  Mount       CLONE_NEWNS       Mount points
  PID         CLONE_NEWPID      Process IDs
  User        CLONE_NEWUSER     User and group IDs
  UTS         CLONE_NEWUTS      Hostname and NIS domain name
```
  - Interacted browser-based sceanrio:
https://www.katacoda.com/courses/docker-security/cgroups-and-namespaces, by Ben Hall
  - **Difficulty** : Beginner
  - **Duration**: 10 Minutes

## Introduction to Seccomp

In this scenario you will learn how to apply Seccomp to reduce the attack surface of containers.
Lab Stpes: 
- Interactive browser-based sceanrio:
https://www.katacoda.com/courses/docker-security/intro-to-seccomp, by Ben Hall
  - **Difficulty** : Beginner
  - **Duration**: 10 Minutes

## Least Previledge Principle

When a Dockerfile doesn’t specify a `USER`, it defaults to executing the container using the `root user`. In practice, there are very few reasons why the container should have root privileges. Docker defaults to running containers using the root user. When that namespace is then mapped to the root user in the running container, it means that the container potentially has root access on the Docker host. Having an application on the container run with the root user further broadens the attack surface and enables an easy path to privilege escalation if the application itself is vulnerable to exploitation.

To minimize exposure, opt-in to create a dedicated user and a dedicated group in the Docker image for the application; use the USER directive in the Dockerfile to ensure the container runs the application with the least privileged access possible.

A specific user might not exist in the image; create that user using the instructions in the Dockerfile.
The following demonstrates a complete example of how to do this for a generic `Ubuntu` image:

```
FROM ubuntu
RUN mkdir /app
RUN groupadd -r lirantal && useradd -r -s /bin/false -g lirantal lirantal
WORKDIR /app
COPY . /app
RUN chown -R lirantal:lirantal /app
USER lirantal
CMD node index.js

``` 
The example above:
- creates a system user `(-r)`, with no password, no home directory set, and no shell
- adds the user we created to an existing group that we created beforehand (using `groupadd`)
- adds a final argument set to the user name we want to create, in association with the group we created.
  
  More Docker best practices are explained [here](https://snyk.io/blog/10-docker-image-security-best-practices/).