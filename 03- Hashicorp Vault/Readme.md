# Secret Management and Data Protection with with HashiCorp Vault
---

## Install Vault [[Ref](https://learn.hashicorp.com/vault/getting-started/install)]
Vault must first be installed on your machine. Vault is distributed as a binary package for all supported platforms and architectures. This page will not cover how to compile Vault from source, but compiling from source is covered in the documentation for those who want to be sure they're compiling source they trust into the final binary.

To install Vault, find the appropriate package for your system and download it. **Vault is packaged as a zip archive**.

After downloading Vault, unzip the package. Vault runs as a single binary named vault. Any other files in the package can be safely removed and Vault will still function.

The final step is to make sure that the `vault` binary is available on the `PATH`. See this page for instructions on setting the `PATH` on Linux and Mac. This page contains instructions for setting the PATH on Windows.
**Verifying the Installation**
After installing Vault, verify the installation worked by opening a new terminal session and checking that the vault binary is available. By executing vault, you should see help output similar to the following:

```
$ vault

Usage: vault <command> [args]

Common commands:
    read        Read data and retrieves secrets
    write       Write data, configuration, and secrets
    delete      Delete secrets and configuration
    list        List data or secrets
    login       Authenticate locally
    server      Start a Vault server
    status      Print seal and HA status
    unwrap      Unwrap a wrapped secret

Other commands:
    audit          Interact with audit devices
    auth           Interact with auth methods
    lease          Interact with leases
    operator       Perform operator-specific tasks
    path-help      Retrieve API help for paths
    policy         Interact with policies
    secrets        Interact with secrets engines
    ssh            Initiate an SSH session
    token          Interact with tokens
```
If you get an error that the binary could not be found, then your PATH environment variable was not setup properly. Please go back and ensure that your PATH variable contains the directory where Vault was installed.

Otherwise, Vault is installed and ready to go!

## Starting the Server [[Ref](https://learn.hashicorp.com/vault/getting-started/dev-server)]

With Vault installed, the next step is to start a Vault server.

Vault operates as a client/server application. The Vault server is the only piece of the Vault architecture that interacts with the data storage and backends. All operations done via the Vault CLI interact with the server over a TLS connection.

In this page, we'll start and interact with the Vault server to understand how the server is started.

**Starting the Dev Server**
First, we're going to start a Vault dev server. The dev server is a built-in, pre-configured server that is not very secure but useful for playing with Vault locally. Later in this guide we'll configure and start a real server.

To start the Vault dev server, run:

```shell
$ vault server -dev

==> Vault server configuration:

             Api Address: http://127.0.0.1:8200
                     Cgo: disabled
         Cluster Address: https://127.0.0.1:8201
              Listener 1: tcp (addr: "127.0.0.1:8200", cluster address: "127.0.0.1:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
               Log Level: (not set)
                   Mlock: supported: false, enabled: false
                 Storage: inmem
                 Version: Vault v1.0.2
             Version Sha: 37a1dc9c477c1c68c022d2084550f25bf20cac33
WARNING! dev mode is enabled! In this mode, Vault runs entirely in-memory
and starts unsealed with a single unseal key. The root token is already
authenticated to the CLI, so you can immediately begin using Vault.

You may need to set the following environment variable:

    $ export VAULT_ADDR='http://127.0.0.1:8200'

The unseal key and root token are displayed below in case you want to
seal/unseal the Vault or re-authenticate.

Unseal Key: 1+yv+v5mz+aSCK67X6slL3ECxb4UDL8ujWZU/ONBpn0=
Root Token: s.XmpNPoi9sRhYtdKHaQhkHP6x

Development mode should NOT be used in production installations!

==> Vault server started! Log data will stream in below:

# ...
```
The dev server stores all its data in-memory (but still encrypted), listens on localhost without TLS, and automatically unseals and shows you the unseal key and root access key.

_Warning: Do not run a dev server in production!_

With the dev server running, do the following four things before anything else:

1. Launch a new terminal session.

2. Copy and run the export VAULT_ADDR ... command from the terminal output. This will configure the Vault client to talk to our dev server.

3. Save the unseal key somewhere. Don't worry about how to save this securely. For now, just save it anywhere.

4. Copy the generated Root Token value and set is as VAULT_DEV_ROOT_TOKEN_ID environment variable:

```shell
$ export VAULT_DEV_ROOT_TOKEN_ID="s.XmpNPoi9sRhYtdKHaQhkHP6x"
```

**Verify the Server is Running**
As instructed, copy and execute `export VAULT_ADDR='http://127.0.0.1:8200'`.

Verify the server is running by running the vault status command. This should succeed and exit with exit code 0.

If it ran successfully, the output should look like the following:

```
$ vault status

Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    1
Threshold       1
Version         1.0.2
Cluster Name    vault-cluster-3cdf26fe
Cluster ID      08082f3a-b58d-1abf-a770-fbb8d87359ee
HA Enabled      false
```
## Your first Secret [[Ref](https://learn.hashicorp.com/vault/getting-started/first-secret)]
Now that the dev server is up and running, let's get straight to it and read and write our first secret.

One of the core features of Vault is the ability to read and write arbitrary secrets securely. On this page, we'll do this using the CLI, but there is also a complete HTTP API that can be used to programmatically do anything with Vault.

Secrets written to Vault are encrypted and then written to backend storage. For our dev server, backend storage is in-memory, but in production this would more likely be on disk or in Consul. Vault encrypts the value before it is ever handed to the storage driver. The backend storage mechanism never sees the unencrypted value and doesn't have the means necessary to decrypt it without Vault.

**Writing a Secret**
Let's start by writing a secret. This is done very simply with the vault kv command, as shown below:

```
$ vault kv put secret/hello foo=world

Key              Value
---              -----
created_time     2019-02-04T19:53:22.730733Z
deletion_time    n/a
destroyed        false
version          1

```
This writes the pair foo=world to the path secret/hello. We'll cover paths in more detail later, but for now it is important that the path is prefixed with secret/, otherwise this example won't work. The secret/ prefix is where arbitrary secrets can be read and written.

You can even write multiple pieces of data, if you want:

```
$ vault kv put secret/hello foo=world excited=yes

Key              Value
---              -----
created_time     2019-02-04T19:54:03.250328Z
deletion_time    n/a
destroyed        false
version          2

```
vault kv put is a very powerful command. In addition to writing data directly from the command-line, it can read values and key pairs from STDIN as well as files. For more information, see the command documentation.

_Warning_: The documentation uses the key=value based entry throughout, but it is more secure to use files if possible. Sending data via the CLI is often logged in shell history. For real secrets, please use files. See the link above about reading in from STDIN for more information.

**Getting a Secret**
As you might expect, secrets can be gotten with vault get:

```shell
$ vault kv get secret/hello

====== Metadata ======
Key              Value
---              -----
created_time     2019-02-04T19:54:03.250328Z
deletion_time    n/a
destroyed        false
version          2

===== Data =====
Key        Value
---        -----
excited    yes
foo        world
```
As you can see, the values we wrote are given back to us. Vault gets the data from storage and decrypts it.

The output format is purposefully whitespace separated to make it easy to pipe into a tool like awk.

This contains some extra information. Many secrets engines create leases for secrets that allow time-limited access to other systems, and in those cases lease_id would contain a lease identifier and lease_duration would contain the length of time for which the lease is valid, in seconds.

To print only the value of a given field:

```shell
$ vault kv get -field=excited secret/hello
yes
```
Optional JSON output is very useful for scripts. For example below we use the jq tool to extract the value of the excited secret:

```shell
$ vault kv get -format=json secret/hello | jq -r .data.data.excited
```
yes
**Deleting a Secret**
Now that we've learned how to read and write a secret, let's go ahead and delete it. We can do this with vault delete:

```shell
$ vault kv delete secret/hello
```
Success! Data deleted (if it existed) at: secret/hello

## Secret Engines [[Ref](https://learn.hashicorp.com/vault/getting-started/secrets-engines)]
Previously, we saw how to read and write arbitrary secrets to Vault. You may have noticed all requests started with secret/. Try the following command which will result an error:

```
$ vault kv put foo/bar a=b

Error making API request.

URL: GET http://localhost:8200/v1/sys/internal/ui/mounts/foo/bar
Code: 403. Errors:

* preflight capability check returned 403, ... grant access to path "foo/bar/"

```
The path prefix tells Vault which secrets engine to which it should route traffic. When a request comes to Vault, it matches the initial path part using a longest prefix match and then passes the request to the corresponding secrets engine enabled at that path. Vault presents these secrets engines similar to a filesystem.

By default, Vault enables a secrets engine called kv at the path secret/. The kv secrets engine reads and writes raw data to the backend storage. Vault supports many other secrets engines besides kv, and this feature makes Vault flexible and unique.

_NOTE_: The kv secrets engine has two versions: `kv` and `kv-v2`. To enable versioned `kv` secrets engine, pass `kv-v2` instead.

This page discusses secrets engines and the operations they support. This information is important to both operators who will configure Vault and users who will interact with Vault.

**Enable a Secrets Engine**
To get started, enable another instance of the kv secrets engine at a different path. Each path is completely isolated and cannot talk to other paths. For example, a kv secrets engine enabled at foo has no ability to communicate with a kv secrets engine enabled at bar.

```shell
$ vault secrets enable -path=kv kv

Success! Enabled the kv secrets engine at: kv/
```

The path where the secrets engine is enabled defaults to the name of the secrets engine. Thus, the following commands are actually equivalent:

```shell
$ vault secrets enable -path=kv kv

$ vault secrets enable kv

```
To verify our success and get more information about the secrets engine, use the vault secrets list command:

```
$ vault secrets list

Path          Type         Accessor              Description
----          ----         --------              -----------
cubbyhole/    cubbyhole    cubbyhole_78189996    per-token private secret storage
identity/     identity     identity_ac07951e     identity store
kv/           kv           kv_15087625           n/a
secret/       kv           kv_4b990c45           key/value secret storage
sys/          system       system_adff0898       system endpoints used for control, policy and debugging

```
This shows there are 4 enabled secrets engines on this Vault server. You can see the type of the secrets engine, the corresponding path, and an optional description (or "n/a" if none was given).

The `sys/` path corresponds to the system backend. These paths interact with Vault's core system and are not required for beginners.

Take a few moments to read and write some data to the new kv secrets engine enabled at kv/. Here are a few ideas to get started:

```
$ vault kv put kv/hello target=world

$ vault kv get kv/hello

$ vault kv put kv/my-secret value="s3c(eT"

$ vault kv get kv/my-secret

$ vault kv delete kv/my-secret
$ vault kv list kv/
```

**Disable a Secrets Engine**
When a secrets engine is no longer needed, it can be disabled. When a secrets engine is disabled, all secrets are revoked and the corresponding Vault data and configuration is removed.

```
$ vault secrets disable kv/

Success! Disabled the secrets engine (if it existed) at: kv/
```
Note that this command takes a PATH to the secrets engine as an argument, not the TYPE of the secrets engine.

Any requests to route data to the original path would result in an error, but another secrets engine could now be enabled at that path.

**What is a Secrets Engine?**
Now that you've successfully enabled and disabled a secrets engine... what is it? What is the point of a secrets engine?

As mentioned above, Vault behaves similarly to a virtual filesystem. The read/write/delete/list operations are forwarded to the corresponding secrets engine, and the secrets engine decides how to react to those operations.

This abstraction is incredibly powerful. It enables Vault to interface directly with physical systems, databases, HSMs, etc. But in addition to these physical systems, Vault can interact with more unique environments like AWS IAM, dynamic SQL user creation, etc. all while using the same read/write interface.

## Web UI [[Ref](https://learn.hashicorp.com/vault/getting-started/ui)]
ault features a user interface (web interface) for interacting with Vault. Easily create, read, update, and delete secrets, authenticate, unseal, and more with the Vault UI.

»
When you start the Vault server in dev mode, Vault UI is automatically enabled and ready to use.

```shell
$ vault server -dev
...

```
Open a web browser and enter `http://127.0.0.1:8200/ui` to launch the UI.

Enter the initial root token to sign 
![ui](images/vault-autounseal-3.png)

**Web UI Wizard**

Vault UI has a built-in guide to navigate you through the common steps to operate various Vault features.
![wizard](»Web UI Wizard Vault UI has a built-in guide to navigate you through the common steps to operate various Vault features.)

## Katacoda : Store Secrets using Hashicorp Vault
In this scenario, we'll explore how to store secrets in Hashicorp Vault. The scenario explains how to initialise a vault, store key/values in a secure way that can later be accessed via the CLI or the HTTP API. The HTTP API is an excellent way to obtain secrets when running inside a Docker Container.
- **Difficulty**: Intermediate 
- **Estimated Time**: 15-20 minutes

### Optional Learning Steps

* **Managing your MySQL Database Secrets with Vault over docker** :  https://github.com/rolivasilva/vault
*  **Spring Boot Secrets by Vault** : https://spring.io/guides/gs/vault-config/