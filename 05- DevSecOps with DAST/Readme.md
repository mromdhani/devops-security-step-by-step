# CI/CD Pipeline With DAST Checks
---

SAST Tools used :
- Apache Tomcat as Application Server
- OWASP ZAP Proxy

### Jenkins Pipeline with ZAP Proxy

```
pipeline {
    agent any
    
    tools {
        maven 'MyMaven' 
    }
    stages {       
      stage ('Check-Git-Secrets In Repository') {
          steps {
              bat 'del secrets-in-git-report 2>nul'
              bat 'docker pull zricethezav/gitleaks'
              bat 'docker run --rm --name=gitleaks zricethezav/gitleaks -v -r https://gitlab.com/mromdhani/mywebappsecurity.git > secrets-in-git-report'
          }
      }
      stage ('Clone') {
            steps {
                git branch: 'master', url: "https://gitlab.com/mromdhani/mywebappsecurity.git"
        }

      }
       stage('Build & Unit test'){
		  steps {
				bat 'mvn clean package';
		      	junit '**/target/surefire-reports/TEST-*.xml'
		      //	archiveArtifacts  'target/*.jar'
	       }
       }
       stage('Security SAST : SonarQube'){
		   steps {
    	   	  bat "mvn sonar:sonar -Dsonar.projectName=mywebappsecurity -Dsonar.projectKey=mywebappsecurity -Dsonar.projectVersion=$BUILD_NUMBER";
		   }
	    }
	   	stage('Security SAST : OWASP Dependency Check'){
	   	    steps {
	   	        //Dependency Plugin is configured in POM.xml descriptor
	   	        bat "mvn verify"
	   	    }
	    }
	    stage('Deploy to Tomcat'){
	   	    steps {
	   	        //Dependency Plugin is configured in POM.xml descriptor
	   	        bat 'curl  -T "target/MyWebAppSecurity.war" "http://admin:admin@localhost:8080/manager/text/deploy?path=/mywebapp&update=true"'
	   	    }
	    }
	    stage('Scan ZAP (Scan Passif)'){
	   	    steps {
	            bat 'docker run -t owasp/zap2docker-stable zap-baseline.py -t  http://localhost:8080/mywebapp/'
            }
	    }
	    }
  }
```